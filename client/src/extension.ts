/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import * as path from 'path';
import * as vscode from 'vscode';
import { TextLine, Range, SourceBreakpoint, Uri, TextEditor, TextDocument, Extension } from 'vscode';
import { BuildInformation, CommandBuild } from "./commands/CommandBuild"
import { CommandRun } from "./commands/CommandRun"
import { CommandDebug } from './commands/CommandDebug';
import { MemoryViewProvider } from './views/MemoryViewProvider'

import ConfigUtils from "./utils/ConfigUtils";
import PathUtils from "./utils/PathUtils";

import { 
	workspace, 
	commands,
	window,
	ExtensionContext,
	extensions 
} from 'vscode';

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	TransportKind
} from 'vscode-languageclient/node';
import ClientUtils from './utils/ClientUtils';

var client: LanguageClient;
var autoBreakpointDetectionActive: Boolean = false;
var lastChangedDate: Date = new Date();
var memoryViewProvider: MemoryViewProvider
var extension : Extension<any>;
var context : ExtensionContext;
var outputChannel : vscode.OutputChannel;
var buildFilename : string;
var viceSymbolFilename : string;
var symbolFilename : string;

export function activate(context: ExtensionContext) {

	extension = extensions.getExtension('paulhocker.kick-assembler-vscode-ext');
	outputChannel = window.createOutputChannel('Kick Assembler Build');
	context = context;

	let serverModule = context.asAbsolutePath(
		path.join('server', 'out', 'server.js')
	);

	let debugOptions = { execArgv: ['--nolazy', '--inspect=6009'] };

	let serverOptions: ServerOptions = {
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: {
			module: serverModule,
			transport: TransportKind.ipc,
			options: debugOptions
		}
	};

	let clientOptions: LanguageClientOptions = {
		documentSelector: ['kickassembler'],
		synchronize: {
			configurationSection: 'kickassembler',
			fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
		}
	};

	context.subscriptions.push(vscode.workspace.onDidChangeConfiguration(configChanged));
	context.subscriptions.push(vscode.workspace.onDidOpenTextDocument(fileOpened)); 
	context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(fileChanged));	
	context.subscriptions.push(vscode.debug.onDidChangeBreakpoints(breakpointsChanged));
	context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(activeTextEditor));
	context.subscriptions.push(vscode.workspace.onDidSaveTextDocument(saveTextDocument));
	context.subscriptions.push(vscode.workspace.onDidCloseTextDocument(closeTextDocument));

	// Create the language client and start the client.
	client = new LanguageClient('kickassembler', 'Kick Assembler', serverOptions, clientOptions);

	// Start the client. This will also launch the server
	let disposable = client.start();

	// Push the disposable to the context's subscriptions so that the 
	// client can be deactivated on extension deactivation
	context.subscriptions.push(disposable);

	//	wait for the language server to be ready
	client.onReady().then(() => {

		//	catch notifications from the language server
		client.onNotification("ERROR", (message: string) => {
			vscode.window.showErrorMessage(message);
		});

		client.onNotification("BUILD", (message: string) => {
			vscode.window.showInformationMessage(message);
		});
		
	});

	let cmdBuild = commands.registerCommand("kickassembler.build", function () {
		commandBuild(ClientUtils.GetOpenDocument(), context, outputChannel);
	});

	let cmdBuildStartup = commands.registerCommand("kickassembler.buildstartup", function () {
		commandBuildStartup(context, outputChannel);
	});

	let cmdBuildRun = commands.registerCommand("kickassembler.buildandrun", function () {
		commandBuildRun(ClientUtils.GetOpenDocument(), context, outputChannel);
	});

	let cmdBuildRunStartup = commands.registerCommand("kickassembler.buildandrunstartup", async function () {
		commandBuildRunStartup(context, outputChannel);
	});

	let cmdBuildDebug = commands.registerCommand("kickassembler.buildanddebug", function () {
		commandBuildDebug(ClientUtils.GetOpenDocument(), context, outputChannel);
	});

	let cmdBuildDebugStartup = commands.registerCommand("kickassembler.buildanddebugstartup", function () {
		commandBuildDebugStartup(context, outputChannel);
	});

	context.subscriptions.push(cmdBuild);
	context.subscriptions.push(cmdBuildStartup);
	context.subscriptions.push(cmdBuildRun);
	context.subscriptions.push(cmdBuildRunStartup);
	context.subscriptions.push(cmdBuildDebug);
	context.subscriptions.push(cmdBuildDebugStartup);

	memoryViewProvider = new MemoryViewProvider(context.extensionUri);
	memoryViewProvider.extContext = context;
	memoryViewProvider.extOutput = outputChannel;

	context.subscriptions.push(vscode.window.registerWebviewViewProvider(MemoryViewProvider.viewType, 	memoryViewProvider));
	
	var _activeDocument = ClientUtils.GetOpenDocument();

	if (_activeDocument) {
		setTimeout(() => {
			saveTextDocument(_activeDocument);
		},1000);
	}

	console.log("Extension (kick-assembler-vscode-ext) client has started.")

	validateSettings();
}

function validateSettings() {

	let settings = vscode.workspace.getConfiguration("kickassembler");
	let _show_notifications:boolean = settings.get("editor.showNotifications");

	var _valid = true;

	if (!ConfigUtils.validateBuildSettings()) {
		_valid = false;
	}

	if (!ConfigUtils.validateRunSettings()) {
		_valid = false;
	}

	if (!ConfigUtils.validateDebugSettings()) {
		_valid = false;
	}
	
	if (_valid) {
		if (_show_notifications) {
			window.showInformationMessage(`Kick AssemblerExtension  ${extension.packageJSON.version}${extension.packageJSON.status} is Ready.`);
		}
	} else {
		window.showWarningMessage(`Kick AssemblerExtension  ${extension.packageJSON.version}${extension.packageJSON.status} is Not Configured Properly. Check your Settings.`);
	}

	// return _valid;
}

export function deactivate(): Thenable<void> {

	if (!client) {
		return undefined;
	}

	console.log("Extension (kickass-vscode-ext) has been deactivated.");
	return client.stop();
}

/**
 * Build a Program From Source
 * @param context 
 * @param output 
 */
function commandBuild(text:TextDocument, context: ExtensionContext, output: vscode.OutputChannel, showOutput : boolean = true): BuildInformation {

	if (!ConfigUtils.validateBuildSettings()) {
		vscode.window.showWarningMessage("Unable to Build your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	var cb = new CommandBuild(context, output, showOutput);
	var _info = cb.build(text, showOutput)
	var targetUri = ClientUtils.GetOpenDocumentUri(true);

	if(_info.buildErrorFile && _info.buildErrorFile !== path.basename(targetUri.path)) {
		targetUri = vscode.Uri.parse(path.dirname(targetUri.path) + path.sep + _info.buildErrorFile);
	}

	client.diagnostics.set(targetUri, _info.buildError);

	let settings = vscode.workspace.getConfiguration("kickassembler");
	if(_info.buildError.length > 0 && settings.get('editor.switchProblemsTabOnError')) {
		vscode.commands.executeCommand('workbench.action.problems.focus')
		.then(function(){
		// Switch back to editor immediatly
			vscode.commands.executeCommand('workbench.action.focusActiveEditorGroup');
		});	
	}

	// look for output filename
	let targetMatch = _info.buildData.match(/.*Writing (prg|d64).*/g);
	for (var i = 0; i < targetMatch.length; i++) {
		let _pos = targetMatch[i].indexOf(":");
		buildFilename = targetMatch[i].substring(_pos + 1).trim();
		break;
	}
	
	// look for output vice symbol file
	let _vice_match = _info.buildData.match(/.*Writing Vice symbol.*/g);
	if (_vice_match) {
		for (var i = 0; i < _vice_match.length; i++) {
			let _pos = _vice_match[i].indexOf(":");
			viceSymbolFilename = _vice_match[i].substring(_pos + 1).trim();
			break;
		}
	}
	
	// look for symbol file
	let _symbol_match = _info.buildData.match(/.*Writing Symbol.*/g);
	if (_symbol_match) {
		for (var i = 0; i < _symbol_match.length; i++) {
			let _pos = _symbol_match[i].indexOf(":");
			symbolFilename = _symbol_match[i].substring(_pos + 1).trim();
			break;
		}
	}
	
	memoryViewProvider.setBuildDataCache(targetUri.path,_info.buildData);
	memoryViewProvider.viewCreate(_info.buildData);
	return _info;
}

async function commandBuildStartup(context: ExtensionContext, output: vscode.OutputChannel, showOutput : boolean = true): Promise<BuildInformation> {

	if (!ConfigUtils.validateBuildSettings()) {
		vscode.window.showWarningMessage("We were unable to Build your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	var _textDocument : TextDocument;
	let settings = vscode.workspace.getConfiguration("kickassembler");
	let _buildStartup : string = settings.get("startup");
	_buildStartup = _buildStartup.trim();

	// if there is no Startup specified, just get out
	if (!_buildStartup) {
		return null;
	}

	// check if startup file exists before trying to build
	let _validBuildStartup : boolean = true;
	_buildStartup = path.join(ClientUtils.GetWorkspaceFolder().uri.fsPath, _buildStartup);

	_validBuildStartup = PathUtils.fileExists(_buildStartup);

	if (_validBuildStartup) {

		_textDocument = await workspace.openTextDocument(_buildStartup);
		var cb = new CommandBuild(context, output, showOutput);
		var _info = cb.buildStartup(_textDocument, showOutput);
		client.diagnostics.set(_textDocument.uri, _info.buildError);
		if(_info.buildError.length > 0 && settings.get('editor.switchProblemsTabOnError')) {
			vscode.commands.executeCommand('workbench.action.problems.focus')
			.then(function(){
			// Switch back to editor immediatly
				vscode.commands.executeCommand('workbench.action.focusActiveEditorGroup');
			});	
		}
		
		// look for output filename
		let targetMatch = _info.buildData.match(/.*Writing (prg|d64).*/g);
		for (var i = 0; i < targetMatch.length; i++) {
			let _pos = targetMatch[i].indexOf(":");
			buildFilename = targetMatch[i].slice(_pos + 1);
			break;
		}
		
		// look for output vice symbol file
		let _vice_match = _info.buildData.match(/.*Writing Vice symbol.*/g);
		if (_vice_match) {
			for (var i = 0; i < _vice_match.length; i++) {
				let _pos = _vice_match[i].indexOf(":");
				viceSymbolFilename = _vice_match[i].substring(_pos + 1).trim();
				break;
			}
		}
		
		// look for symbol file
		let _symbol_match = _info.buildData.match(/.*Writing Symbol.*/g);
		if (_symbol_match) {
			for (var i = 0; i < _symbol_match.length; i++) {
				let _pos = _symbol_match[i].indexOf(":");
				symbolFilename = _symbol_match[i].substring(_pos + 1).trim();
				break;
			}
		}
		
		memoryViewProvider.setBuildDataCache(_buildStartup, _info.buildData);
		memoryViewProvider.viewCreate(_info.buildData);
		return _info;

	} else if(showOutput) {

		var _valid = vscode.window.showWarningMessage("The Setting for the Startup file is invalid.", { title: 'Open Settings'});
		_valid.then((value) => {
			if (value){
				vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.startup`);
			}
		});

	}

	return null;
}

/**
 * Build and then Run the currently Open file in the Editor.
 * @param context 
 * @param output 
 */
function commandBuildRun(text : TextDocument, context : ExtensionContext, output : vscode.OutputChannel) {

	if (!ConfigUtils.validateRunSettings()) {
		vscode.window.showWarningMessage("We were unable to Run your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	saveOpenDocument( function(){

		if (commandBuild(text, context, output).buildStatus == 0) {
			commandRun(context, output);
		}
	});
}

/**
 * Build and Run the Startup program
 * @param context 
 * @param output 
 */
async function commandBuildRunStartup(context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateRunSettings()) {
		vscode.window.showWarningMessage("We were unable to Run your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	saveOpenDocument(function(){

		commandBuildStartup(context, output).then(buildInformation  => {
			if (buildInformation.buildStatus == 0) {
				commandRunStartup(context, output);
			}
		});

	});
}

/**
 * Build and Debug the Currently Open Document
 * @param context 
 * @param output 
 */
function commandBuildDebug(text : TextDocument, context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateDebugSettings()) {
		vscode.window.showWarningMessage("We were unable to Debug your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	saveOpenDocument(function(){

		if (commandBuild(text, context, output).buildStatus == 0) {
			commandDebug(context, output);
		}
	});
}

/**
 * Build and Debug the Startup Program
 * @param context 
 * @param output 
 */
function commandBuildDebugStartup(context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateDebugSettings()) {
		vscode.window.showWarningMessage("We were unable to Debug your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	saveOpenDocument(function(){

		commandBuildStartup(context, output).then(buildInformation  => {
			if (buildInformation.buildStatus == 0) {
				commandDebugStartup(context, output);
			}
		});

	});
}

/**
 * Run the Currently Open Program
 * @param context 
 * @param output 
 */
function commandRun(context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateRunSettings()) {
		vscode.window.showWarningMessage("We were unable to Run your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	var cr = new CommandRun(context, output);
	cr.buildFilename = buildFilename;
	cr.viceSymbolFilename = viceSymbolFilename;
	cr.symbolFilename = symbolFilename;
	cr.runOpen();
}

/**
 * Run The Startup Program
 * @param context 
 * @param output 
 */
function commandRunStartup(context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateRunSettings()) {
		vscode.window.showWarningMessage("We were unable to Run your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	var cr = new CommandRun(context, output);
	cr.buildFilename = buildFilename;
	cr.viceSymbolFilename = viceSymbolFilename;
	cr.symbolFilename = symbolFilename;
	cr.runStartup();
}

function commandDebug(context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateDebugSettings()) {
		vscode.window.showWarningMessage("We were unable to Debug your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	var cd = new CommandDebug(context, output);
	cd.buildFilename = buildFilename;
	cd.viceSymbolFilename = viceSymbolFilename;
	cd.symbolFilename = symbolFilename;
	cd.runOpen();
}

function commandDebugStartup(context: ExtensionContext, output: vscode.OutputChannel) {

	if (!ConfigUtils.validateDebugSettings()) {
		vscode.window.showWarningMessage("We were unable to Debug your program because there was a problem validating your Settings. Please check your Settings and Try Again.");
		return;
	}

	var cd = new CommandDebug(context, output);
	cd.buildFilename = buildFilename;
	cd.viceSymbolFilename = viceSymbolFilename;
	cd.symbolFilename = symbolFilename;
	cd.runStartup();
}

function configChanged(e:vscode.ConfigurationChangeEvent) {
	let affected = e.affectsConfiguration("kickassembler");
	ConfigUtils.validateBuildSettings();
	memoryViewProvider.viewInit();	
}

function _addBreakpoints(text:vscode.TextDocument, checkLineNumber?:number) {

	let _doc = text;
	autoBreakpointDetectionActive = true;
	var line:TextLine;
	var breakExpressionInfo:RegExpMatchArray;

	// Remove all existing breakpoints
	if(!checkLineNumber){
		let bpexists = vscode.debug.breakpoints.filter((bp:SourceBreakpoint) => {
			return bp.location.uri.path == _doc.uri.path;
		});
		if(bpexists.length > 0){
			vscode.debug.removeBreakpoints(bpexists);
		}
	}

	//find all existing breakpoints and create them in vscode	

	let newBreakpoints: vscode.Breakpoint[] = [];

	for(var i=(checkLineNumber || 0),iL=(checkLineNumber ? checkLineNumber+1 : _doc.lineCount);i<iL;i++){
		line = _doc.lineAt(i);
		let checkLine = line.text.trim();
		let existingBreak = checkLine.match(/^(\/\/\s*)*\.break/);
		let existingPrint = checkLine.match(/^(\/\/\s*)*\.print(\s+[\(\"]*|\s*[\(\"]+)[\w")]+/);		
		let existingPrintNow = checkLine.match(/^(\/\/\s*)*\.printnow(\s+[\(\"]*|\s*[\(\"]+)[\w")]+/);
		if(existingBreak || existingPrint || existingPrintNow) {
			breakExpressionInfo = existingBreak ? checkLine.slice(existingBreak[0].length).trim().match(/".*"/) : undefined;
			newBreakpoints.push(new vscode.SourceBreakpoint(
				new vscode.Location(_doc.uri, new vscode.Position(i, 0)),
				checkLine.slice(0,2) != "//",
				breakExpressionInfo ? breakExpressionInfo[0] : '',
				'',
				existingPrint ? checkLine.slice(checkLine.indexOf(".print")+6).trim() :
				existingPrintNow ? checkLine.slice(checkLine.indexOf(".printnow")+9).trim() : ''
			));
		}
		// remove a possible existing breakpoint
		let bpexists = vscode.debug.breakpoints.filter((bp:SourceBreakpoint) => {
			return bp.location.uri.path == _doc.uri.path && bp.location.range.start.line === i;
		});
		if(bpexists.length > 0){
			vscode.debug.removeBreakpoints(bpexists);
		}
	}
	if(newBreakpoints.length>0) {
		vscode.debug.addBreakpoints(newBreakpoints);
	}

	autoBreakpointDetectionActive = false;
}

function closeTextDocument(text : TextDocument) {

	// when not working on extension document, get out
	if (text.languageId != 'kickassembler' ) { return; }
	
	memoryViewProvider.viewCreate("");
}


function fileChanged(e:vscode.TextDocumentChangeEvent){

	// when not working on extension file, get out!
	if (e.document.languageId != 'kickassembler') { return; }

	let isSameChangeLine = e.contentChanges[0].range.start.line === e.contentChanges[0].range.end.line;
	let rangeToCheck = isSameChangeLine && !e.contentChanges[0].text.includes("\n") ? e.contentChanges[0].range.start.line : undefined;
	lastChangedDate = new Date();

	_addBreakpoints(e.document,rangeToCheck);
}

/**
 * Save the currently open document if available. * 
 */
function saveOpenDocument(callback?:Function) {

	// only when open active document is available

	if (window && window.activeTextEditor && window.activeTextEditor.document) {
		// save the active document and return
		window.activeTextEditor.document.save().then(function (reponse) {
			if(callback) callback();
		});
	} else if(callback) callback();

}

function saveTextDocument(text:TextDocument) {

	// when not working on extension document, get out
	if (text.languageId != 'kickassembler' ) { return; }

	// we need a full build when the memory viewer is visible to make the memoryViewer work properly
	let memoryViewVisible = !memoryViewProvider.webviewView || memoryViewProvider.webviewView.visible;
	let settings = vscode.workspace.getConfiguration("kickassembler");
	let triggerSetting:string = settings.get('autoAssembleTrigger');
	let startupSetting:string = settings.get('startup');
	startupSetting = startupSetting.trim();
	if (memoryViewVisible && triggerSetting.indexOf('onSave') !== -1 && (!startupSetting || commandBuildStartup(context, outputChannel, false) === null)) {
		commandBuild(text, context, outputChannel, false);
	}
}

function fileOpened(text:TextDocument, checkLineNumber?:number) {

	// when not working on extension document, get out
	if (text.languageId != 'kickassembler' ) { return; }

	_addBreakpoints(text, checkLineNumber);
}

function activeTextEditor(editor:TextEditor) {

	if (!editor) { return; }

	// when not working on extension document, get out
	if (editor.document.languageId != 'kickassembler' ) { return; }

	let cachedBuildData = memoryViewProvider.getBuildDataCache(editor.document.uri.path);
	if (cachedBuildData === false) {
		// when switched to an already open but never build file
		saveTextDocument(editor.document);
	} else {
		memoryViewProvider.viewCreate(cachedBuildData);
	}
}

function breakpointsChanged(breakpointChanges:vscode.BreakpointsChangeEvent){

	let editor = vscode.window.activeTextEditor;

	// no document, get out
	if (!editor) { return; }

	// when not working on extension document, get out
	if (editor.document.languageId != 'kickassembler' ) { return; }

	if (!vscode.workspace.getConfiguration("kickassembler").get("editor.breakpointsUseBreakDirective")) { return; }

	if (editor) {

		let document = editor.document;
	// Don't add/remove anything if it wasn't triggered by native context menu
		if(!autoBreakpointDetectionActive){

			breakpointChanges.added.forEach((bp:vscode.SourceBreakpoint) => {
				if(bp.location.uri.path == document.uri.path) {
					let bpLine = document.lineAt(bp.location.range.start.line);
					if(bpLine && !bpLine.text.trim().match(/^(\/\/\s*)*\.(break|print)/)) {
						editor.edit(editBuilder => {
							editBuilder.insert(
								bp.location.range.start,
								(bp.enabled===false ? "// ":"")+(bp.logMessage ? ".print "+bp.logMessage : ".break"+(bp.condition ? ' "'+bp.condition+'"':""))+"\n"
							);
						});
					}
				}
			});
			breakpointChanges.removed.forEach((bp:vscode.SourceBreakpoint) => {
				if(bp.location.uri.path == document.uri.path) {
					let bpLine = document.lineAt(bp.location.range.start.line);
					if(bpLine && bpLine.text.trim().match(/^(\/\/\s*)*\.(break|print)/)){
						let conditionString = bpLine.text.trim().replace(/^(\/\/\s*)*\.(break|printnow|print)\s*/,'');
						// only remove if the current line matches EXACTLY the remove object (otherwise it's an old trigger when typing quickly and should be ignored!)
						// also make sure it wasn't triggered internally by vscode because of quick key repetition (so basically only the contextmenu remove will match here)
						if ((conditionString == bp.condition || conditionString == bp.logMessage) && (new Date()).getTime() - lastChangedDate.getTime() > 500){
							editor.edit(editBuilder => {
								editBuilder.delete(
									new Range(bp.location.range.start.line,0,bp.location.range.start.line+1,0)
								);
							});
						}
					}
				}
			});
		}
// changes always happen automatically by vscode when inserting from clipboard or new lines/del lines happen
// But only single bp changes are triggered via contextmenu
		if(breakpointChanges.changed.length === 1){
			breakpointChanges.changed.forEach((bp:vscode.SourceBreakpoint) => {
				if(bp.location.uri.path == document.uri.path) {
					let bpLine = document.lineAt(bp.location.range.start.line);
					if(bpLine && bpLine.text.trim().match(/^(\/\/\s*)*\.(break|print)/)){
						let spaceRepeat = Math.max(bpLine.text.trim().replace(/^(\/\/\s*)*\.(break|printnow|print)/,'').search(/[^\s]/),1);
						editor.edit(editBuilder => {
							editBuilder.replace(
								new Range(bp.location.range.start.line,0,bp.location.range.start.line,bpLine.text.length+2),
								(bp.enabled===false ? "//" : "") + " ".repeat(bpLine.text.replace(/^\/\//,'').search(/\S/)) + (bp.logMessage ? (bpLine.text.match(/\.printnow/) ? ".printnow" : ".print") + " ".repeat(spaceRepeat) + bp.logMessage : ".break" + (bp.condition ? " ".repeat(spaceRepeat) + '"' + bp.condition.replace(/^"(.*)"$/, '$1') +'"' : ''))
							);
						});
					}
				}
			});
		}
	}
}
